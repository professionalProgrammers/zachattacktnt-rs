const addon = require('../native');
const https = require('https');
const BASE_URL = 'https://nhentai.net';
const RAND_URL = BASE_URL + '/random/';

function get(url) {
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            let body = '';
            res.on('data', (d) => {
                body += d;
            });

            res.on('end', () => {
                resolve({
                    res,
                    body
                });
            });
        });
    });
}

async function getRandomComic() {
    let {
        res,
        body
    } = await get(RAND_URL);

    let loc = BASE_URL + res.headers.location;
    let data;
    while (loc) {
        data = await get(loc);
        loc = data.res.headers.loc;
    }

    let comic = new Comic(data.body);
    return comic;
}

class Comic {
    constructor(data) {
        this.inner = new addon.Comic(data);
    }
	
	getId() {
		return this.inner.getId();
	}

    getMediaId() {
        return this.inner.getMediaId();
    }

    getPageLinks() {
        return this.inner.getPageLinks();
    }
}

module.exports.getRandomComic = getRandomComic;
module.exports.Comic = Comic
