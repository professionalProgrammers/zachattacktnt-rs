extern crate neon;
extern crate nhentai;

use neon::prelude::*;
use nhentai::Comic;
use nhentai::Document;

declare_types! {
    pub class JsComic for Comic {
        init(mut cx) {
            let data = cx.argument::<JsString>(0)?.value();

            let doc = Document::from(data.as_str());
            let maybe_comic = Comic::from_doc(&doc);

            Ok(maybe_comic.unwrap())
        }

        method getId(mut cx){
            let this = cx.this();
            let id = {
                let guard = cx.lock();
                let comic = this.borrow(&guard);
                comic.get_id()
            };
            Ok(cx.number(id as f64).upcast())
        }

        method getMediaId(mut cx){
            let this = cx.this();
            let id = {
                let guard = cx.lock();
                let comic = this.borrow(&guard);
                comic.get_media_id().to_string()
            };
            Ok(cx.string(&id).upcast())
        }

        method getPageLinks(mut cx){
            let this = cx.this();
            let urls = {
                let guard = cx.lock();
                let comic = this.borrow(&guard);
                comic.iter_resolved_page_urls().map(|u| u.into_string()).collect::<Vec<_>>()
            };
            Ok(neon_serde::to_value(&mut cx, &urls)?)
        }
  }
}

register_module!(mut m, {
    m.export_class::<JsComic>("Comic")?;
    Ok(())
});
